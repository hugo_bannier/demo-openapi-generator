# OpenAPI Generator Demo

## Purpose

Demo how to automate generation of client code from a Spring Rest Controller

## Architecture

### Api

Contains the code of the API (Spring Rest controller + associated DTO)

### Typescript-client-generator

Generator of typescript code using angular to query Api

### Java-client-generator

Generator of java using native class (configurable) to query Api

### Functional-test

Functional test for API using generated jar from Java-client-generator

## Requirements

* Java 17+
* Maven 3

## Commands

Generate OpenAPI file

```shell
cd api && mvn verify
```

Generate client in Java

```shell
cd java-client-generator && mvn install
```

Generate client in Typescript

```shell
cd typescript-client-generator && mvn package
```

Run functional test (you have to run API first)

```shell
cd functional-test && mvn test
```
