package bzh.penher.openapigenerator.controller;

import bzh.penher.openapigenerator.dto.CommuneDto;
import bzh.penher.openapigenerator.dto.CreateCommuneRequest;
import bzh.penher.openapigenerator.dto.ValueWrapper;
import bzh.penher.openapigenerator.enumeration.CommuneType;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = "/commune", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommuneController {

    List<CommuneDto> communeDtos = List.of(
            new CommuneDto(UUID.randomUUID(), "Le Vieux-Bourg", CommuneType.SMALL),
            new CommuneDto(UUID.randomUUID(), "Saint-Bihy", CommuneType.SMALL),
            new CommuneDto(UUID.randomUUID(), "Saint-Brandan", CommuneType.SMALL),
            new CommuneDto(UUID.randomUUID(), "Saint-Gildas", CommuneType.SMALL)
    );


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CommuneDto> getCommunes() {
        return communeDtos;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ValueWrapper<UUID> createCommune(@RequestBody final CreateCommuneRequest payload) {
        val communeDto = new CommuneDto(UUID.randomUUID(), payload.name(), payload.communeType());


        this.communeDtos = Stream
                .concat(Stream.of(communeDto), communeDtos.stream())
                .sorted(Comparator.comparing(CommuneDto::getName))
                .toList();

        return new ValueWrapper<>(communeDto.getId());
    }
}
