package bzh.penher.openapigenerator.dto;

import bzh.penher.openapigenerator.enumeration.CommuneType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommuneDto {
    private UUID id;
    private String name;
    private CommuneType communeType;
}
