package bzh.penher.openapigenerator.dto;

import bzh.penher.openapigenerator.enumeration.CommuneType;

public record CreateCommuneRequest(String name, CommuneType communeType) {}
