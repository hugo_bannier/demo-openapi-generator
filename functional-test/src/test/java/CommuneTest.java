import bzh.penher.openapigenerator.ApiException;
import bzh.penher.openapigenerator.client.CommuneControllerApi;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.openapitools.client.model.CreateCommuneRequest;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;


class CommuneTest {
    final CommuneControllerApi communeControllerApi = new CommuneControllerApi();

    @Test
    void shouldRetrieveAllCommunes() throws ApiException {
        // arrange & act
        val response = communeControllerApi.getCommunesWithHttpInfo();

        // assert
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertFalse(response.getData().isEmpty());
    }

    @Test
    void shouldCreateCommune() throws ApiException {
        // arrange
        val communes = communeControllerApi.getCommunes();
        val payload = new CreateCommuneRequest().name("Quintin").communeType(CreateCommuneRequest.CommuneTypeEnum.MEDIUM);

        // act
        val response = communeControllerApi.createCommuneWithHttpInfo(payload);

        // assert
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
        val expectedId = response.getData().getValue();
        assertNotNull(expectedId);

        val updatedCommunes = communeControllerApi.getCommunes();
        assertEquals(communes.size() + 1, updatedCommunes.size());

        assertTrue(
                updatedCommunes
                        .stream()
                        .filter(commune -> commune.getName() != null)
                        .filter(commune -> commune.getId() != null)
                        .filter(commune -> commune.getName().equals(payload.getName()))
                        .anyMatch(commune -> commune.getId().equals(expectedId))
        );
    }
}
